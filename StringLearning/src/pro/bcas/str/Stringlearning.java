package pro.bcas.str;

public class Stringlearning {
	public static void main(String[]args) {
		System.out.println("1 String Method:char charAt(int index)");
		String name = "MY NAME IS KATHUSHA";
		char result = name.charAt(8);
		System.out.println(name);
		System.out.println(result);
		System.out.println("---------------------------------------");
		
		
		System.out.println("5 String Method:String concat(String str)");
		String s = "MY NAME IS KATHUSHA ";
		s = s.concat (" MY FRIENDS CALL ME KAATHU");
		System.out.println(s);
		System.out.println("---------------------------------------");
	
		
		System.out.println("16 String Method:int indexOf(int ch)");
		String ch = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println("Found Index :"+ch.indexOf('W'));
		System.out.println("---------------------------------------");
		
		System.out.println("17 String Method:int indexOf(int ch,int fromIndex)");
		String Str = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println(" Found Index :"+Str.indexOf('W',5));
		System.out.println("---------------------------------------");
		
		System.out.println("18 String Method:int indexOf(String Str)");
		String str = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println("MY NAME IS KATHUSHA IT WAS THE NAME ");
		String SubStr1 = new String ("NAMES");
		System.out.println("Found Index :" + Str.indexOf(SubStr1));
		System.out.println("---------------------------------------");
		
		
		System.out.println("19 String Method:int indexOf(String str,fromIndex)");
		String fromIndex = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println(fromIndex);
		System.out.println(SubStr1);
		System.out.println("Found Index :" + Str.indexOf(SubStr1,10));
		System.out.println("---------------------------------------");
		
		
		System.out.println("21 String Method:int lastindexOf(int ch)");
		String lastIndex = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println(lastIndex);
		System.out.println("Found last Index :" + Str.lastIndexOf('t'));
		System.out.println("---------------------------------------");
		
		System.out.println("22 String Method:int lastIndexOf(int ch,int fromIndex)");
		System.out.println(lastIndex);
		System.out.println("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println("Found last Index:"+Str.lastIndexOf('I',5));
		System.out.println("---------------------------------------");
		
		System.out.println("23 String Method:int lastIndexOf(String str)");
		String SubStr2 = new String ("NAMES");
		System.out.println("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println(lastIndex);
		System.out.println(SubStr2);
		System.out.println("Found last Index:"+Str.lastIndexOf(SubStr2));
		System.out.println("---------------------------------------");
		
		System.out.println("24 String Method:int lastIndexOf(String str,int fromIndex)");
		String SubStr3 = new String ("NAMES");
		System.out.println("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println("Found last Index:"+Str.lastIndexOf(SubStr3));
		System.out.println("---------------------------------------");
		
		
		System.out.println("25 String Method:int Length()");
		String Str1 = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		String Str2 = new String ("NAMES");
		System.out.println("String Length :");
		System.out.println(Str1.length ());
		System.out.println("String Length :");
		System.out.println(Str2.length ());
		System.out.println("---------------------------------------");
		
		
		
		System.out.println("29 String Method:String replace(char oldChar,char,nweChar)");
		String replace = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println(replace);
		System.out.println("Return Value  :");
		System.out.println(Str.replace  ('o','T'));
		System.out.println(" Return Value :");
		System.out.print(Str.replace ('1','o'));
		System.out.println("---------------------------------------");
		
		
		
		
		System.out.println("34 String Method: boolean starts(String prefix)");
		String booleanWith = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println(booleanWith);
		System.out.println("Return Value  :");
		System.out.println(Str.startsWith ("MY NAME IS KATHUSHA"));
		System.out.println(" Return Value :");
		System.out.println(Str.startsWith ("IT WAS THE NAME"));
		System.out.println("---------------------------------------");
		
		
		
		System.out.println("37 String Method: String substring(int beginIndex)");
		String StrSubStr = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println(StrSubStr);
		System.out.println("Return Value  :");
		System.out.println(Str.substring (10));
		System.out.println("---------------------------------------");
		
		
		
		
		System.out.println("38 String Method: String substring(int beginIndex, int endIndex)");
		String StrSubStr1  = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println( StrSubStr);
		System.out.println("Return Value  :");
		System.out.println(StrSubStr.substring (10,15));
		System.out.println("---------------------------------------");
		
		
		
		
		System.out.println("40 String Method: String toLowerCase()");
		String LowerCase = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println(LowerCase);
		System.out.println("Return Value  :");
		System.out.println(Str.toLowerCase());
		System.out.println("---------------------------------------");
		
		
		
		
		System.out.println("43 String Method: String toUpperCase()");
		String UpperCase = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println(Str.toUpperCase());
		System.out.println("Return Value  :"+Str.toUpperCase());
		System.out.println("---------------------------------------");
		
		
		
		
		System.out.println("45 String Method: String trim()");
		String Strtrim = new String ("MY NAME IS KATHUSHA IT WAS THE NAME ");
		System.out.println(Str.trim());
		System.out.println("Return Value  : "+Str.trim());
		System.out.println     ("---------------------------------------");
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
				
	}
}
		